import requests
from urllib.parse import urlparse, urljoin, unquote
from bs4 import BeautifulSoup
import colorama

# init the colorama module
colorama.init()

GREEN = colorama.Fore.GREEN
GRAY = colorama.Fore.LIGHTBLACK_EX
RESET = colorama.Fore.RESET
YELLOW = colorama.Fore.YELLOW

# initialize the set of links (unique links)
internal_urls = []

total_urls_visited = 0


def is_valid(url):
    """
    Checks whether `url` is a valid URL.
    """
    parsed = urlparse(url)
    return bool(parsed.netloc) and bool(parsed.scheme)


def get_all_website_links(url):
    """
    Returns all URLs that is found on `url`
    in which it belongs to the same website
    """
    # all URLs of `url`
    urls = set()
    # domain name of the URL without the protocol
    domain_name = urlparse(url).netloc
    soup = BeautifulSoup(requests.get(url).content, "html.parser")
    for a_tag in soup.findAll("a"):
        href = a_tag.attrs.get("href")
        if href == "" or href is None:
            # href empty tag
            continue
        # join the URL if it's relative (not absolute link)
        href = urljoin(url, href)
        parsed_href = urlparse(href)
        # remove URL GET parameters, URL fragments, etc.
        href = parsed_href.scheme + "://" + parsed_href.netloc + parsed_href.path
        if not is_valid(href):
            # not a valid URL
            continue
        if href in internal_urls:
            # already in the set
            continue
        print(f"{GREEN}[*] Arquivo: {href}{RESET}")
        urls.add(href)
        internal_urls.append(href)
    return urls


def crawl(url, max_urls=30):
    """
    Crawls a web page and extracts all links.
    You'll find all links in `internal_urls` global set variables.
    params:
        max_urls (int): number of max urls to crawl, default is 30.
    """
    global total_urls_visited
    total_urls_visited += 1
    print(f"{YELLOW}[*] Testando: {url}{RESET}")
    links = get_all_website_links(url)
    for link in links:
        if total_urls_visited > max_urls:
            break
        crawl(link, max_urls=max_urls)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Link Extractor Tool")
    parser.add_argument("url", help="The URL to extract links from.")
    parser.add_argument("-m", "--mode", choices=["movie", "serie"], default='movie')

    args = parser.parse_args()
    url = args.url
    mode = args.mode

    crawl(url, 0)

    print("[+] Total links:", len(internal_urls))

    # save the internal links to a file
    if mode == 'movie':
        idx = -2
    else:
        idx = -3

    llist = []

    llist.append("#EXTM3U")

    for internal_link in internal_urls:
        if len(internal_link.strip().split('/')) == len(args.url.split('/')):
            llist.append(f"#EXTINF:-1,{unquote(internal_link.strip().split('/')[-1]).split('.')[0]}")
            llist.append(internal_link.strip())
        else:
            pass


    with open(f"{unquote(url.split('/')[idx])}.m3u", "w", encoding="utf-8") as f:
        f.write('\n'.join(llist))
